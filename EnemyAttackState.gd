extends State
class_name EnemyAttackState

var move_comp

var shots_fired : = 0
var max_shots : = 1

func _init(actor).("EnemyAttack", actor):
	move_comp = actor.node.movement
	max_shots = 1 + randi() % 4

func enter(actor):
	print("entering enemy attack")
	if shots_fired >= max_shots:
		actor.die()
		finish()
		return
	
	if actor.node.aim_position == Vector2.ZERO:
		finish()
		return
	actor.node.animate("Attack")
	actor.node.attack()
	shots_fired += 1

func on_input(actor, input):
	if input == "Attack":
		print("starting next attack")
		shots_fired += 1
		next_attack(actor)

func next_attack(actor):
	if shots_fired >= max_shots:
		# Fly to exit, when move state finishes
		# this state will kill the actor
		actor.node.fly_exit()
	else:
		actor.node.attack()

func on_vector_input(actor, vector):
	if vector == Vector2.ZERO:
		return
	actor.node.brain.push_goal(MoveState.new(actor))
	
func update(actor, delta):
	pass

func physics_process(actor, delta):
	pass

func is_finished(actor)->bool:
	return _finished

func finish():
	_finished = true

func interrupt():
	pass

func exit(actor):
	pass

func animation_finished(anim_name : String):
	pass
