extends Node2D

signal on_system_break(sys_id)
signal on_repaired(sys_id)

const MAX_FUEL : = 20
const FUEL_USE_RATE : = 2.0
var fuel : = 0
var timer : = 0.0

const MAX_HP : = 500.0
var hitpoints : = 1.0

const MAX_AMMO : = 10
var ammo : = 1

func _ready() -> void:
	connect("on_system_break", get_parent(), "on_system_break")
	connect("on_repaired", get_parent(), "on_repair")
	fuel = MAX_FUEL
	hitpoints = MAX_HP
	ammo = MAX_AMMO

func update_stats(delta: float) -> void:
	if timer >= FUEL_USE_RATE:
		timer = 0.0
		_consume_fuel()
	else:
		timer += delta
		
func add_item(sys_id : int, quantity : = 10):
	match(sys_id):
		Globals.SHIP_SYSTEM.FuelPump:
			_add_fuel(quantity)
		Globals.SHIP_SYSTEM.Weapons:
			_add_ammo(quantity)
		_:
			# a repair item was inserted
			# i.e. microchip, reactor pellet
			emit_signal("on_repaired", sys_id)

func consume_item(sys_id : int):
	match(sys_id):
		Globals.SHIP_SYSTEM.FuelPump:
			_consume_fuel()

func _add_fuel(quantity : int = 10):
	if fuel <= 0 and quantity > 0:
		emit_signal("on_repaired", Globals.SHIP_SYSTEM.FuelPump)
	fuel += quantity
	
func _consume_fuel():
	if fuel <= 0:
		emit_signal("on_system_break", Globals.SHIP_SYSTEM.FuelPump)
		return
	fuel = max(0, fuel - 1)

func _add_ammo(quantity : int):
	ammo = min(ammo + quantity, MAX_AMMO)
	emit_signal("on_repaired", Globals.SHIP_SYSTEM.Weapons)

func consume_ammo():
	ammo = max(
		0,
		ammo - 1
	)

func take_damage(dmg : float = 1.0):
	hitpoints = clamp(
		hitpoints - dmg,
		0.0,
		MAX_HP
	)
	if hitpoints == 0:
		var death_evt = Event.new("on_ship_death", hitpoints)
		death_evt.fire()

