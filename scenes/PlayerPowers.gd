extends Node2D

var parent
export(float) var max_dist : = 300.0
var target : Node2D

func _ready():
	parent = get_parent()

func use_power(target_pos : Vector2):
	var dist = parent.global_position.distance_to(target_pos)
	if dist >= max_dist:
		return
	target = parent.get_target_at_mouse()
	if target and target.actor.actor_state != Actor.ACTOR_STATE.Held:
		target.apply_power(0, 2.0)
#	var dist = parent.global_position.distance_to(target_pos)
#	if dist >= max_dist:
#		stop_use()
#	elif verify_target() and target.actor.actor_state == Actor.ACTOR_STATE.Held:
#		continue_hold(target_pos)
#		return
#
#	if parent.can_see(target_pos) == false:
#		return
#
#
#	target = parent.get_target_at_mouse()
#	if !target: return
#	if target.actor.actor_state == Actor.ACTOR_STATE.Held:
#		target.follow_target(target_pos)
#	else:
##		print("player has a target at mouse")
#		target.brain.push_goal(HoldState.new(target.actor))

func verify_target():
	if !target: return false
	if is_instance_valid(target):
		return true
	target = null
	return false

func continue_hold(target_pos):
	target.follow_target(target_pos)
	

func stop_use():
	print("power stopped")
#
#	if verify_target():
#		target.follow_target(Vector2.ZERO)
#		target.brain.finish_current()
#		print("power stopped")
		
