extends Node2D

var noise : OpenSimplexNoise
var time = 0

func _ready():
	noise = OpenSimplexNoise.new()
	noise.seed = rand_range(-100, 100)

func _process(delta: float) -> void:
	time += delta * 75
	var offset = noise.get_noise_1d(time)
	position = Vector2(2.0 * offset/12, 2.0 * offset/12)
