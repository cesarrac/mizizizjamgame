extends %BASE%
class_name MyState

func _init(actor).("MyName", actor):
%TS%pass

func enter(actor):
%TS%pass

func on_input(actor, input):
%TS%pass

func on_vector_input(actor, vector):
%TS%pass
	
func update(actor, delta):
%TS%pass

func physics_process(actor, delta):
%TS%pass

func is_finished(actor)->bool:
%TS%return _finished

func finish():
%TS%_finished = true

func interrupt():
%TS%pass

func exit(actor):
%TS%pass

func animation_finished(anim_name : String):
%TS%pass
