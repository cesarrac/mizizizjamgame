extends Reference
class_name Actor

enum ACTOR_STATE {Dead, Active, Dead, Held}

var id : int
var actor_name : = "Name"
var node : Node2D
var callbacks : = []
var actor_state : = 1

func _init(
	_id : int,
	_name : String
):
	id = _id
	actor_name = _name
	actor_state = ACTOR_STATE.Active

func set_node(_node : Node2D):
	node = _node

func die():
	free_node()

func free_node():
	if !node: return
	node.queue_free()
	node = null
	
