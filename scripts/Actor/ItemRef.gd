extends Reference
class_name ItemRef

var item_type : = 0
var sprite_frame : = 0

func _init(_type : int):
	item_type = _type
	set_sprite_frame()

func set_sprite_frame():
	match(item_type):
		Globals.ITEM_TYPES.Cargo:
			sprite_frame = 7
		Globals.ITEM_TYPES.Fuel:
			sprite_frame = 0
		Globals.ITEM_TYPES.ReactorPellet:
			sprite_frame = 4
		Globals.ITEM_TYPES.Microchip:
			sprite_frame = 6
		Globals.ITEM_TYPES.FriedMicrochip:
			sprite_frame = 1
		Globals.ITEM_TYPES.Ammo:
			sprite_frame = 5

