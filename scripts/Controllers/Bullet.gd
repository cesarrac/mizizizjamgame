extends KinematicBody2D

export(float) var speed : = 300.0
var velocity : = Vector2.ZERO
var direction : = Vector2.ZERO

func set_bullet(mask : int, target_position : Vector2):
#	$Area2D.collision_mask = mask
	direction = (target_position - global_position).normalized()
	
func _physics_process(delta):
	velocity = direction * speed
	velocity = move_and_slide(velocity)


func _on_Area2D_body_entered(body):
	queue_free()
