extends Polygon2D

onready var tween = $Tween
var scaler : = 0.1 setget set_scale
const DURATION : = 0.5

var fx_lstnr

func _ready() -> void:
	fx_lstnr = EventListener.new("new_danger", self)
	fx_lstnr.subscribe_to("on_new_danger", "CanvasFX")

func new_danger(sys_id : int):
	fill_in(Color(1.0, 0.0, 0.0, 0.3))

func fill_in(new_color = Color.white):
	if tween.is_active():
		return
	self_modulate = new_color
	scale = Vector2(0.1, 0.1)
	show()
	tween.interpolate_property(
		self,
		"scaler",
		scaler,
		1.0,
		DURATION,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	tween.start()

func set_scale(value):
	scaler = value
	scale = Vector2(scaler, scaler)

func _on_Tween_tween_all_completed() -> void:
	print("tween done")
	hide()
	scaler = 0.1
