extends KinematicBody2D
class_name Character_Controller

onready var body_sprite = $Main_Sprite
onready var occlusion_ray : RayCast2D = $Occlusion_Ray
onready var animator = $AnimationPlayer
onready var brain = $Brain
onready var movement = $Movement
onready var hitbox = $Hitbox
#onready var wpn_control = $Weapon

export(int) var sprite_anim_frame : int = 0
var cur_base_sprite : int = 0

var input_vector : = Vector2.ZERO
var last_dir : = Vector2.ZERO

var actor

#var main_weapon : Weapon

func set_up():

	cur_base_sprite = 0

	actor = Actor.new(
		randi() % 1000,
		name
	)
	actor.set_node(self)
	brain.set_actor(actor)

func set_input_vector(vector : Vector2):
	last_dir = input_vector
	input_vector = vector
	brain.set_input_vector(vector)
#	input_vector = vector.normalized()
#	if input_vector == Vector2.ZERO:
#		animator.play("Idle")
#	else:
#		animator.play("Run")

func on_input(input):
	brain.set_input(input)

func move(input_vector, delta):
	movement.move(input_vector, delta)

func animate(anim_name : String):
	animator.play(anim_name)

func receive_attack(attack):
	print("Character receives attack")
	
func can_see(target_position : Vector2)->bool:
	occlusion_ray.cast_to = (target_position - global_position)
	occlusion_ray.force_raycast_update()
	if occlusion_ray.is_colliding():
		return false
	return true

#func equip_weapon(wpn):
#	main_weapon = wpn
