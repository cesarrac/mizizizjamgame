extends StaticBody2D


export(Globals.ITEM_TYPES) var item_type
var break_listen
var repair_listen
onready var warning = $Warning_Indicator

func _ready() -> void:
	break_listen = EventListener.new("on_sys_break", self)
	break_listen.subscribe_to("on_sys_break", "Dispenser_%s" % item_type)
	repair_listen = EventListener.new("on_sys_repair", self)
	repair_listen.subscribe_to("on_sys_repair", "Dispenser_%s" % item_type)

func on_sys_repair(sys_id):
	if _is_sys_match(sys_id)== false:
		return
	warning.hide()

func on_sys_break(sys_id):
	if _is_sys_match(sys_id) == false:
		return
	warning.indicate()

func _is_sys_match(sys_id)->bool:
	match(sys_id):
		Globals.SHIP_SYSTEM.Engines:
			if item_type == Globals.ITEM_TYPES.ReactorPellet:
				return true
		Globals.SHIP_SYSTEM.Nav:
			if item_type == Globals.ITEM_TYPES.Microchip:
				return true
		Globals.SHIP_SYSTEM.Weapons:
			if item_type == Globals.ITEM_TYPES.Ammo:
				return true
		Globals.SHIP_SYSTEM.FuelPump:
			if item_type == Globals.ITEM_TYPES.Fuel:
				return true
				
	return false
		

func interact(interactor : Node2D):
	dispense(interactor)

func dispense(interactor : Node2D):
	var evt = Event.new("item_request", [item_type, interactor])
	evt.fire()
