extends StaticBody2D

export(int) var door_open_frame : = 0
var start_frame : = 0
onready var sprite = $Sprite
onready var interactable = $Interactable
onready var collider = $CollisionShape2D
var is_open : = false

func _ready() -> void:
	interactable.connect("interact_ended", self, "interact_ended")
	start_frame = sprite.frame

func interact(interactor : Node2D):
	if is_open == true: return
	# open door
	sprite.frame = door_open_frame
	is_open = true
	collider.disabled = true

func interact_ended():
	if is_open == false: return
	# close door
	sprite.frame = start_frame
	is_open = false
	call_deferred("enable_collision")

func enable_collision():
	collider.disabled = false
