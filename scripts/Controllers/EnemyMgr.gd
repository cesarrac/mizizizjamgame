extends Node2D

onready var enemy_scn = preload("res://scenes/Enemy.tscn")
const MIN_LEFT = 80
const MAX_LEFT = 128
const MAX_RIGHT = 848
const MIN_RIGHT = 720
const SPAWN_Y = 650
var start_y : float
var end_y : float
var last_position : = Vector2.ZERO
const EXIT_Y = -250

var cur_count : = 0
var total_count : = 0
var y_sort_node
var spawning : = false
var ship_area : Rect2
var enemy_lstnr

func _ready() -> void:
	var floor_map : TileMap = get_parent().get_node("Floors")
	y_sort_node = get_parent().get_node("Walls/YSort")
	ship_area = floor_map.get_used_rect()
	start_y = ship_area.position.y * 16
	end_y = ship_area.end.y * 16
	$Spawn_Timer.connect("timeout", self, "_spawn")
	
	enemy_lstnr = EventListener.new("start_spawn", self)
	enemy_lstnr.subscribe_to("on_enemy_spawn", "EnemyMgr")

func start_spawn(count : = 4):
	if spawning: return
	print("starting spwn!")
	spawning = true
	total_count = count
	cur_count = 0
	$Spawn_Timer.start()

func _spawn():
	print("spawning enemy!")
	var enemy = enemy_scn.instance()
	var target_position = get_position()
	y_sort_node.add_child(enemy)
	enemy.global_position = Vector2(target_position.x, SPAWN_Y)
	enemy.set_target_position(target_position)
	enemy.set_aim_position(get_ship_position())
	enemy.exit_position = Vector2(target_position.x, EXIT_Y)
	if target_position.x > MAX_LEFT:
		enemy.body_sprite.flip_h = true
	
	cur_count += 1
	if cur_count < total_count:
		$Spawn_Timer.start()
	else:
		spawning = false
		$Spawn_Timer.stop()

func get_ship_position()->Vector2:
	return Vector2(
		rand_range(ship_area.position.x + 16, ship_area.end.x - 16),
		rand_range(ship_area.position.y + 16, ship_area.end.y - 16)
	) * 16
	
func get_position()->Vector2:
	# generate 4 position options
	var positions = []
	# 2 on left
	positions.append(Vector2(
		rand_range(MIN_LEFT, MAX_LEFT),
		rand_range(start_y, end_y)
	))
	positions.append(Vector2(
		rand_range(MIN_LEFT, MAX_LEFT),
		rand_range(start_y, end_y)
	))
	# 2 on right
	positions.append(Vector2(
		rand_range(MIN_RIGHT, MAX_RIGHT),
		rand_range(start_y, end_y)
	))
	positions.append(Vector2(
		rand_range(MIN_RIGHT, MAX_RIGHT),
		rand_range(start_y, end_y)
	))
	# eliminate last pos if in array
	var i = positions.find(last_position)
	if i >= 0:
		positions.remove(i)
	positions.shuffle()
	var pos = positions.back()
	last_position = pos
	return pos
