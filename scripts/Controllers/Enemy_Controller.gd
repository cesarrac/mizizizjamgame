extends Character_Controller
class_name Enemy_Controller

onready var detect_zone = $Detection_Zone
onready var attack_timer = $Attack_Timer
onready var laser = $Laser
const FOLLOW_DEAD_ZONE : = 4.0

#var main_weapon : Weapon

var is_attacking : = false

var target_position : = Vector2.ZERO
var aim_position : = Vector2.ZERO
var exit_position : = Vector2.ZERO

func _ready():
	.set_up()

func _physics_process(delta):
#	seek_target()
	if input_vector != Vector2.ZERO:
		check_distance()

func check_distance():
	var dist = global_position.distance_to(target_position)
	if dist <= 16.0:
		set_input_vector(Vector2.ZERO)
		if is_attacking:
			brain.push_goal(EnemyAttackState.new(actor))
			is_attacking = false

	else:
		set_input_vector(target_position - global_position)
	
func set_target_position(target : Vector2):
	target_position = target
	set_input_vector(target_position - global_position)

func set_aim_position(aim : Vector2):
	aim_position = aim
	is_attacking = true

func attack():
	if attack_timer.is_stopped() == false:
		return
	# Do attack and start timer
	laser.set_aim(aim_position)
	laser.shoot = true
	
	var ship_dmg_evt = Event.new("on_ship_damaged", [self, 1 + randi() % 4])
	ship_dmg_evt.fire()
	# When timer finishes it calls on_input("Attack")
	# to do a new attack or finish attack state
	attack_timer.start()

func fly_exit():
	print("fly exit!")
	laser.shoot = false
	if exit_position == Vector2.ZERO:
		print("ERROR - Enemy %s had no exit pos so it just got deleted!")
		actor.die()
	else:
		# this pushes move state and comes back to kill actor
		
		target_position = exit_position
		set_input_vector(target_position - global_position)
		brain.push_goal(MoveState.new(actor))

func _on_Attack_Timer_timeout():
	on_input("Attack")
