extends Area2D

signal interact_started
signal interact_ended

func disable():
	monitoring = false
	
func enable():
	monitoring = true

func _on_Interactable_body_entered(body):
	if body.has_node("Interactor"):
		body.get_node("Interactor").start_interact(self)
		emit_signal("interact_started")


func _on_Interactable_body_exited(body):
	if body.has_node("Interactor"):
		body.get_node("Interactor").stop_interaction(self)
		emit_signal("interact_ended")

func interact(interactor : Node2D):
	var parent = get_parent()
	if parent.has_method("interact") == false:
		print("ERROR No interact method implemented in %s" % parent.filename)
		return
	parent.interact(interactor)
