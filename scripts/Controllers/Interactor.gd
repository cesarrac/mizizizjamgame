extends Node2D

var interact_item : Node2D

func _ready():
	get_parent().get_node("PlayerInput").connect("on_input", self, "on_input")

func start_interact(item):
	interact_item = item

func stop_interaction(item):
	interact_item = null

func on_input(input):
	if input == "Interact":
		do_interact()
	elif input == "Drop":
		drop()

func drop():
	if get_child_count() == 0:
		return
	get_parent().drop()
#	get_child(0).drop(get_parent().get_parent())
	
func do_interact():
	if interact_item == null or is_instance_valid(interact_item) == false:
		interact_item = null
		return
	interact_item.interact(self)
	interact_item = null

func remove_item():
	if get_child_count() == 0:
		return
	get_child(0).remove()
	
		
