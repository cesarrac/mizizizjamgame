extends KinematicBody2D

onready var sprite = $Sprite
onready var interactable = $Interactable
onready var collider = $CollisionShape2D
var item_ref
var on_remove : FuncRef

func set_up(_ref):
	item_ref = _ref
	$Sprite.frame = item_ref.sprite_frame

func interact(interactor : Node2D):
	if global_position.distance_to(interactor.global_position) > 64:
		interactor.stop_interact()
		return
	# pick up
	if interactor.get_child_count() > 0:
		print("Can only pick up one item at a time!")
		return
	reparent(interactor)

	disable()

func reparent(new_parent : Node2D):
	get_parent().remove_child(self)
	new_parent.add_child(self)
	set_owner(new_parent)
	global_position = new_parent.global_position

func drop(world_parent : Node2D, drop_pos : Vector2):
	reparent(world_parent)
	global_position = drop_pos
	enable()

func disable():
	collider.disabled = true
	interactable.disable()

func enable():
	collider.disabled = false
	interactable.enable()

func remove():
	if on_remove:
		on_remove.call_func(self)
		return
	queue_free()
	
