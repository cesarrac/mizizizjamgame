extends RayCast2D


onready var line : Line2D = $Line2D
onready var tween : Tween = $Tween
onready var hit_particles : Particles2D = $Hit_Particles
onready var origin_particles : Particles2D = $Origin_Particles
onready var beam_particles : Particles2D = $Beam_Particles
onready var light : Light2D = $Hit_Particles/Light2D

var shoot : = false setget set_shoot

func _ready():
	set_physics_process(false)
	line.points[1] = Vector2.ZERO

func set_aim(aim_pos : Vector2):
	cast_to = (aim_pos - global_position).normalized()
	cast_to *= 1000

func set_shoot(value):
	shoot = value
	origin_particles.emitting = shoot
	beam_particles.emitting = shoot
#	light.enabled = shoot
	if shoot == true:
		appear()
	else:
		hit_particles.emitting = false
		disappear()
	set_physics_process(shoot)
	
func _physics_process(delta):
	var cast_point : = cast_to
	force_raycast_update()
	
	if is_colliding():
		cast_point = to_local(get_collision_point())
		hit_particles.emitting = true
		hit_particles.global_rotation = get_collision_normal().angle()
		hit_particles.position = cast_point
		light.enabled = true
#		light.global_rotation = get_collision_normal().angle()
#		light.position = cast_point * 0.5
#		light.scale = Vector2((cast_point.length() * 0.5) / 32, 1)
		
	else:
		hit_particles.emitting = false
		light.enabled = false
	
	line.points[1] = cast_point
	# set particles along beam to half way to cast point
	beam_particles.position = cast_point * 0.5
	beam_particles.process_material.emission_box_extents.x = cast_point.length() * 0.5
	
	
	
func appear():
	tween.stop_all()
	tween.interpolate_property(
		line,
		"width",
		0.0,
		4.0,
		0.2)
	tween.start()

func disappear():
	tween.stop_all()
	tween.interpolate_property(
		line,
		"width",
		4.0,
		0.0,
		0.16)
	tween.start()
