extends Node2D

onready var debug = $CanvasLayer/Debug_Display
onready var wall_map = $Walls
onready var star_scn = preload("res://scenes/Star.tscn")
onready var star_holder = $Floors/ParallaxBackground/ParallaxLayer/Stars
onready var ship = $Ship
onready var item_mgr = $ItemMgr
onready var player_start = $PLAYER_START
onready var player_scn = preload("res://scenes/Player.tscn")
onready var camera_scn = preload("res://scenes/Components/PlayerCamera.tscn")
var player : Node2D
var player_cam : Camera2D

func _enter_tree() -> void:
	randomize()

func _ready() -> void:
	debug.add_stat("FPS", Engine, "get_frames_per_second", true)
	

	spawn_stars()
	
	ship.init_ship($Walls/YSort, 1)
	
	item_mgr.init_cargo($Floors, $Walls/YSort)

	spawn_player()
	
#	Sound_Mgr.play_music("cute_bass")

func spawn_player():
	player = player_scn.instance()
	player.global_position = player_start.global_position
	$Walls/YSort.add_child(player)
	# add camera
	player_cam = camera_scn.instance()
	player.add_child(player_cam)
	player_cam.global_position = player.global_position
	player_cam.current = true

func spawn_stars():
	var ship_rect : Rect2 = wall_map.get_used_rect()
	var start = ship_rect.position - Vector2(16,16)
	var end = ship_rect.end + Vector2(16, 16)
	var max_stars : = 500
	var star_count : = 0
	for i in range(max_stars):
		var pos = Vector2(
			rand_range(start.x, end.x),
			rand_range(start.y, end.y)
		)
		if ship_rect.has_point(pos):
			continue
		var star = star_scn.instance()
		star.global_position = pos * 16
		star_holder.add_child(star)
#	var star_start_left = ship_rect.position - Vector2(32, 32)
#	var star_start_right = ship_rect.position + Vector2(ship_rect.size.x, 0)
#	var max_stars : = 500
#	var star_count : = 0
#	for i in range(200):
#			var star = star_scn.instance()
#			var pos = Vector2(
#				clamp(
#					star_start_left.x + (randi() % 32),
#					star_start_left.x,
#					star_start_left.x + 32
#				),
#				star_start_left.y + (randi() % int(ship_rect.size.y + 32))
#
#			)
#			star.global_position = pos * 16
#			star_holder.add_child(star)
#	for i in range(200):
#			var star = star_scn.instance()
#			var pos = Vector2(
#				clamp(
#					star_start_right.x + (randi() % 16),
#					star_start_right.x,
#					star_start_right.x + 32
#				),
#				star_start_right.y + (randi() % int(ship_rect.size.y + 32))
#
#			)
#			star.global_position = pos * 16
#			star_holder.add_child(star)
		
#	for x in range(star_start_left.x, star_end.x):
#		for y in range(star_start_left.y, star_end.y):
#			if star_count 
#			var pos = Vector2(x, y) * 16

#			star_count += 1
