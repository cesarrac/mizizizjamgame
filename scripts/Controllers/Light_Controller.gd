extends Sprite

var noise : OpenSimplexNoise
var time = 0
onready var light = $Light
#const MAX_SCALE = 12.0
#const MIN_SCALE = 3.0
#var scaler = 1

func _ready():
	noise = OpenSimplexNoise.new()
	noise.seed = rand_range(-100, 100)
	
func _process(delta):
	time += delta * 75
	var offset = noise.get_noise_1d(time)
	light.scale = Vector2(0.5 + offset/12, 0.5 + offset/12)
#	scaler += delta * 40
#	var noise_value = noise.get_noise_1d(scaler)
#	if noise_value < 0.0:
#		noise_value *= -1
#
#	var s = max(MIN_SCALE, MAX_SCALE * noise_value)
##	var y = max(MIN_SCALE, MAX_SCALE * noise_value)
#
#	texture_scale = s#3 + noise_value

