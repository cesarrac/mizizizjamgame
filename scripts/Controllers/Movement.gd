extends Node2D


var body : KinematicBody2D
var speed : = 0.0
var velocity : = Vector2.ZERO
export(float) var max_speed = 500.0
export(float) var acceleration : = 400.0
export(float) var friction : = 600.0

func _ready():
	speed = max_speed
	body = get_parent()

func move(input_vector, delta):
	if input_vector != Vector2.ZERO:
		velocity = velocity.move_toward(input_vector * speed, acceleration * delta)
	else:
		velocity = velocity.move_toward(Vector2.ZERO, friction * delta)
		
	velocity = body.move_and_slide(velocity)
