extends Character_Controller
class_name Player_Controller

onready var powers = $Powers
onready var target_ray : RayCast2D = $Targeting_Ray
onready var bullet_scn = preload("res://scenes/Bullet.tscn")
onready var interactor = $Interactor

onready var step_scn = preload("res://scenes/Components/Step_Trail.tscn")
const STEP_TIME = 0.2
var step_timer : = 0.0

#var main_weapon : Weapon
func _ready():
	.set_up()

func animate(anim_name : String):
	animator.play("Player" + anim_name)

func _process(delta: float) -> void:
	if movement.velocity == Vector2.ZERO:
		return
	if step_timer >= STEP_TIME:
		step_timer = 0.0
		spawn_step_fx()
	else:
		step_timer += delta
	
func on_input(input):

#	if input == "M0":
#		$Laser.set_aim(get_global_mouse_position())
#		$Laser.shoot = true
#		print("shooting")
#	elif input == "M0_Up":
#		$Laser.shoot = false
	.on_input(input)
	
func drop():
	# drop item on hand if any
	if interactor.get_child_count() == 0:
		return
	# cast in 4 directions checking for walls
	var dirs = Globals.DIRECTIONS
	var drop_pos = global_position
	for i in range(dirs.size()):
		occlusion_ray.cast_to = dirs[i] * 16
		occlusion_ray.force_raycast_update()
		if occlusion_ray.is_colliding() == false:
			drop_pos = global_position + (dirs[i] * 16)
			break
	interactor.get_child(0).drop(get_parent(), drop_pos)

func use_power():
	powers.use_power(get_global_mouse_position())
	var bullet = bullet_scn.instance()
	bullet.global_position = global_position
	get_parent().add_child(bullet)
	bullet.set_bullet(collision_mask, get_global_mouse_position())

func stop_power():
	powers.stop_use()

func get_target_at_mouse()->Node2D:
	var space_state = get_world_2d().direct_space_state
	var hit = space_state.intersect_point(
		get_global_mouse_position(),
		1,
		[self],
		target_ray.collision_mask
	)
	if hit:
		return hit[0].collider
	return null

func spawn_step_fx():
	var step = step_scn.instance()
	step.global_position = global_position + Vector2(0, 8) # + (last_dir * -8)
	if last_dir.x < 0:
		step.flip_h = true
	get_parent().add_child(step)
#	print("step spawned!")
	
