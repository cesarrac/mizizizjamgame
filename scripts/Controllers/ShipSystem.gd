extends StaticBody2D
class_name ShipSystem

var system_id : = 0
var system_state : = 0
onready var warning = $Warning_Indicator
signal on_interact(id, interactor)

func set_up(sprite_frame : int = 0):
	$Sprite.frame = sprite_frame
	system_state = Globals.SYSTEM_STATE.Active

func interact(interactor : Node2D):
	emit_signal("on_interact", system_id, interactor)

func repair():
	turn_on()
	print("system repaired!")
	var repair_evt = Event.new("on_sys_repair", system_id)
	repair_evt.fire()

func breakdown():
	system_state = Globals.SYSTEM_STATE.Broken
	warning.show()
	var break_evt = Event.new("on_sys_break", system_id)
	break_evt.fire()

func shut_down():
	system_state = Globals.SYSTEM_STATE.Inactive
	warning.show()

func turn_on():
	system_state = Globals.SYSTEM_STATE.Active
	warning.hide()

