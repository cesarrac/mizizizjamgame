extends Node2D

onready var sys_scn = preload("res://scenes/ShipSystem.tscn")
onready var ship_stats = $Ship_Stats
onready var ship_danger_mgr = $ShipDanger_Mgr

const SYS_POSITIONS = [
	Vector2(552, 358),
	Vector2(456, 358),
	Vector2(216, 168),
	Vector2(232, 264),
	
	
]
const SYSTEM_SPRITE_FRAME = [
	6,
	9,
	7,
	3
	
]

var ship_systems : = []
var danger_lstnr
var ship_state : int
var damage_lstner

var enemy_target : Node2D
var laser

func _ready() -> void:
	laser = get_parent().get_node("Laser")
	danger_lstnr = EventListener.new("on_new_danger", self)
	danger_lstnr.subscribe_to("on_new_danger", "Ship")
	damage_lstner = EventListener.new("on_ship_damaged", self)
	damage_lstner.subscribe_to("on_ship_damaged", "Ship")

func init_ship(y_sort_node : Node2D, cur_state : int = 0):
	ship_state = cur_state
	# recall: order is - Fuel, Engine, Nav, Weapons
	for i in range(4):
		var sys = sys_scn.instance()
		sys.global_position = SYS_POSITIONS[i]
		y_sort_node.add_child(sys)
		sys.system_id = i
		sys.set_up(SYSTEM_SPRITE_FRAME[i])
		ship_systems.append(sys)
		sys.connect("on_interact", self, "on_sys_interact")
	
		# TEST
	on_system_break(Globals.SHIP_SYSTEM.Nav)
		# test
	on_new_danger(Globals.SHIP_SYSTEM.Weapons)

func _process(delta: float) -> void:
	if ship_state != Globals.SHIP_STATE.Travelling:
		return
	ship_danger_mgr.update_danger(delta)
	ship_stats.update_stats(delta)


func on_sys_interact(sys_id : int, interactor : Node2D):
	match(sys_id):
		Globals.SHIP_SYSTEM.Engines:
			interact_engines(sys_id, interactor)
		Globals.SHIP_SYSTEM.Nav:
			interact_nav(sys_id, interactor)
		Globals.SHIP_SYSTEM.FuelPump:
			interact_fuel(sys_id, interactor)
		Globals.SHIP_SYSTEM.Weapons:
			interact_weapons(sys_id, interactor)

func interact_engines(sys_id, interactor : Node2D):
	if interactor.get_child_count() == 0:
		return
	var item = interactor.get_child(0)
	if item.item_ref.item_type == Globals.ITEM_TYPES.ReactorPellet:
		ship_stats.add_item(sys_id)
		interactor.remove_item()

func interact_nav(sys_id, interactor : Node2D):
	print("interact_nav")
	if interactor.get_child_count() == 0:
		if ship_systems[sys_id].system_state != Globals.SYSTEM_STATE.Active:
			# create a fried microchip and give it to player
			var evt = Event.new("item_request", [Globals.ITEM_TYPES.FriedMicrochip, interactor])
			evt.fire()
			ship_systems[sys_id].shut_down()
		return
	if ship_systems[sys_id].system_state != Globals.SYSTEM_STATE.Inactive:
		print("nav system needs to dispense fried microchip before taking in new one")
		return
	var item = interactor.get_child(0)
	if item.item_ref.item_type == Globals.ITEM_TYPES.Microchip:
		ship_stats.add_item(sys_id)
		interactor.remove_item()	

func interact_fuel(sys_id, interactor : Node2D):
	print("interact_fuel")
	if interactor.get_child_count() == 0:
		return
	var item = interactor.get_child(0)
	if item.item_ref.item_type == Globals.ITEM_TYPES.Fuel:
		# add fuel
		ship_stats.add_item(sys_id, 10)
		interactor.remove_item()
		
func interact_weapons(sys_id, interactor : Node2D):
	print("interact_weapons")
	if interactor.get_child_count() == 0:
		return
	var item = interactor.get_child(0)
	if item.item_ref.item_type == Globals.ITEM_TYPES.Ammo:
		ship_stats.add_item(sys_id, 1)
		interactor.remove_item()

func on_system_break(sys_id : int):
	if sys_id < 0 or sys_id >= ship_systems.size():
		return
	ship_systems[sys_id].breakdown()
	# do cam shake
	var trauma_evt = Event.new("on_trauma", 0.5)
	trauma_evt.fire()
	print("system %s breaking down!" % sys_id)
	match(sys_id):
		Globals.SHIP_SYSTEM.Engines:
			break_engines()
		Globals.SHIP_SYSTEM.Nav:
			break_nav()
		Globals.SHIP_SYSTEM.FuelPump:
			break_fuel()
		_:
			break_fuel()
		
func break_fuel():
	# stop thrusters
	toggle_thrusters(false)
	
func break_engines():
	# stop thrusters
	toggle_thrusters(false)

func break_nav():
	# TOA to station becomes longer
	pass

func break_weapons():
	# Disable weapons
	
	# Ship takes damage
	ship_stats.take_damage(1.0 + randi() % 11)

func on_repair(sys_id : int):
	ship_systems[sys_id].repair()
	# handle effect of repair on ship
	match(sys_id):
		Globals.SHIP_SYSTEM.FuelPump:
			# turn on thrusters
			toggle_thrusters(true)
		Globals.SHIP_SYSTEM.Engines:
			# turn on thrusters
			toggle_thrusters(true)

func toggle_thrusters(on : bool):
	var thrusters = get_node("Thrusters").get_children()
	for t in thrusters:
		t.emitting = on

func on_new_danger(sys_id : int):
	match(sys_id):
		Globals.SHIP_SYSTEM.Weapons:
			# spawn enemies
			var enemy_evt = Event.new("on_enemy_spawn", 2)
			enemy_evt.fire()

		Globals.SHIP_SYSTEM.Engines:
			break_engines()
			ship_systems[sys_id].breakdown()
		Globals.SHIP_SYSTEM.Nav:
			break_nav()
			ship_systems[sys_id].breakdown()
		Globals.SHIP_SYSTEM.FuelPump:
			ship_stats.consume_item(sys_id)
			# once fuel is consumed, if it <=0
			# it will call breakdown on the fuel system
	
func on_ship_damaged(args):
	print("ship received damage!")
	enemy_target = args[0]
	# take damage and attack
	ship_stats.take_damage(args[1])
	fire_laser()

func fire_laser():
	if ship_stats.ammo <= 0: return
	if $Attack_Timer.is_stopped() == false:
		return
	ship_stats.consume_ammo()
	laser.set_aim(enemy_target.global_position)
	laser.shoot = true
	# start attack timer
	$Attack_Timer.start()

func _on_Attack_Timer_timeout():
	laser.shoot = false
	$Attack_Timer.stop()
	print("stopping ship shooting")
