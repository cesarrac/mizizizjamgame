extends Sprite

const MAX_DURATION = 32.0

var fade_duration : = 6.0
var max_alpha : = 1.0
var alpha : = 0.0
var fading_in : = true
var noise : OpenSimplexNoise
var time = 0

func _ready() -> void:
	noise = OpenSimplexNoise.new()
	noise.seed = rand_range(-100, 100)
	var max_frame = hframes * vframes
	frame = randi() % max_frame
	fade_duration = rand_range(6.0, MAX_DURATION)
	max_alpha = rand_range(0.6, 1.0)
	fade_in()

func fade_in():
	fading_in = true
	$Tween.interpolate_property(
		self,
		"alpha",
		alpha,
		max_alpha,
		fade_duration,
		Tween.TRANS_ELASTIC,
		Tween.EASE_IN_OUT
	)
	$Tween.start()

func fade_out():
	fading_in = false
	$Tween.interpolate_property(
		self,
		"alpha",
		alpha,
		0.0,
		fade_duration,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	$Tween.start()

func _process(delta: float) -> void:
	self_modulate.a = alpha
	time += delta * 75
	var offset = noise.get_noise_1d(time)
	scale = Vector2(0.5 + offset/4, 0.5 + offset/4)

func _on_Tween_tween_all_completed() -> void:
	if fading_in:
		fade_out()
	else:
		fade_in()
