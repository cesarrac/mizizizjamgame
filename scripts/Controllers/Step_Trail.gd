extends Sprite

const MAX_SCALE = 1.0

const LIFE_TIME = 0.8
var _timer : = 0.0

func _ready() -> void:
	$AnimationPlayer.play("stretch&squash")

#func _process(delta: float) -> void:
#	if _timer >= LIFE_TIME:
#		_timer = 0.0
#		queue_free()
#	else:
#		_timer += delta


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	queue_free()
#	print("step finished")
