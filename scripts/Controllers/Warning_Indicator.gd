extends Sprite

onready var anim = $AnimationPlayer

func show():
	.show()
	anim.play("Warning")

func indicate():
	.show()
	anim.play("Indicate")
