extends VBoxContainer

onready var label : Label = $Label
var stats : Array

func _ready():
	stats = []

func add_stat(stat_name : String, object, stat_ref : String, is_method : bool):
	stats.append(
		{
			"name" : stat_name,
			"obj" : object,
			"ref" : stat_ref,
			"is_method" : is_method
		}
	)

func _process(delta):
	run()

func run():

	var label_text = ""
		# Display Static Memory always
	var mem = OS.get_static_memory_usage()
	label_text += str("Static Memory:", String.humanize_size(mem))
	label_text += "\n"

	for s in stats:
		var value = null
		if s["obj"] == null or is_instance_valid(s["obj"]) == false:
			continue
		if s["is_method"] == true:
			value = s["obj"].call(s["ref"])
		else:
			value = s["obj"].get(s["ref"])
		label_text += str(s["name"], ": ", value)
		label_text += "\n"
	label.text = label_text
	
