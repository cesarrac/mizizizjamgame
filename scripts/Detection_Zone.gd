extends Area2D


var target : Node2D setget set_target, get_target

func set_target(t):
	if is_instance_valid(t) == false:
		return
	target = t
	
func get_target():
	if is_instance_valid(target) == false:
		return null
	return target


func _on_Detection_Zone_body_entered(body):
	target = body

func _on_Detection_Zone_body_exited(body):
	if target and body == target:
		target = null
