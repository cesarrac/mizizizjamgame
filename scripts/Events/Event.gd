extends Reference
class_name Event

var event_name : String = "An Event"
#var listeners = []
var value

var kill_on_fire : bool = true

func _init(_name : String, _value, _kill_after : bool = true):
	event_name = _name   
	value = _value
	kill_on_fire = _kill_after


func fire():
	EventMgr.fire_event(self)
