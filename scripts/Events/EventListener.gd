extends Reference
class_name EventListener

var func_to_execute : FuncRef
var target
var actor_id

func _init(func_name, func_obj):
	func_to_execute = funcref(func_obj, func_name)

func subscribe_to(event_name : String, id):
	target = event_name
	actor_id = id
	EventMgr.subscribe(event_name, self, id)

func on_event(value):
#	print("Event listened by listener")
	if func_to_execute:
		func_to_execute.call_func(value)
