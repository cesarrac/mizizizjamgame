extends Node2D

#const FUNC_STR = "on_"
#var events = []
var listeners : Dictionary

func _enter_tree():
	listeners = {}

# USING GLOBAL EVENTS CUSTOM CLASS:
func fire_event(event):
	var fired = false
	if listeners.has(event.event_name) == false or listeners[event.event_name].size() <= 0:
#		print("EventMgr has no subscribers for event %s" % event.event_name)
		return
	var l_array = listeners[event.event_name]
	for l in l_array:
		if l.target == event.event_name:
			l.on_event(event.value)
			fired = true
	
	if !fired:
		print("event %s fired but no one was listening!" % event.event_name)
		return
	# remove event after fire
#	if event.kill_on_fire:
#		remove_event(event)
	
#func new_event(event):
#	events.append(event)
##	print("event registered")

func subscribe(event_name, listener, _actor_id):
	if listeners.has(event_name) == false:
		listeners[event_name] = []
	listeners[event_name].append(listener)
#	print("%s subscribed to %s" % [actor_id, event_name])

func unsub(actor_id):
	var rmv = {}
	var id_type = typeof(actor_id)
	for evt_name in listeners.keys():
		for i in range(listeners[evt_name].size()):
			var this_id = listeners[evt_name][i].actor_id
			if typeof(this_id) != id_type: continue
			if this_id == actor_id:
				if rmv.has(evt_name) == false:
					rmv[evt_name] = []
				rmv[evt_name].append(i)
				
	for evt_name in rmv.keys():
		for i in range(rmv[evt_name].size()):
			listeners[evt_name].remove(rmv[evt_name][i])

func remove_listener(listener):
	if listeners.has(listener.target) == false:
		return
	var i = listeners[listener.target].find(listener)
	if i < 0 : return
	listeners[listener.target].remove(i)
	if listeners[listener.target].size() <= 0:
		listeners.erase(listener.target)

func clear_all_listeners():
	listeners.clear()

func _exit_tree():
	clear_all_listeners()
	queue_free()
	
