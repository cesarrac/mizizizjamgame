extends Node2D

enum SHIP_SYSTEM {FuelPump, Engines, Nav, Weapons}

enum SYSTEM_STATE {Inactive, Active, Broken}

enum SHIP_STATE {Docked, Travelling, Stopped}

const DIRECTIONS = [
	Vector2.UP,
	Vector2.RIGHT,
	Vector2.DOWN,
	Vector2.LEFT
]

enum ITEM_TYPES {
	Cargo,
	Fuel,
	ReactorPellet,
	Microchip,
	FriedMicrochip,
	Ammo
}
