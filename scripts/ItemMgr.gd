extends Node2D

onready var item_scn = preload("res://scenes/Item.tscn")
var y_sort_node
var floor_tiles : TileMap
const CARGO_TILE = 4
const CARGO_AREA_WIDTH = 6
const CARGO_AREA_HEIGHT = 5
const POS_OFFSET = Vector2(8, 8)
var cargo_tiles : = []
var last_cargo_pos : = Vector2.ZERO

var cur_cargo : = []

var itm_request

func _ready() -> void:
	itm_request = EventListener.new("item_request", self)
	itm_request.subscribe_to("item_request", "ItemMgr")

func item_request(args : Array):
	var itm_type = args[0]
	var itm_holder = args[1]
	var itm_ref = ItemRef.new(itm_type)
	var itm = new_item(itm_ref, itm_holder.global_position, itm_holder)

func init_cargo(_floor_tilemap, y_sort, max_cargo : = 10):
	floor_tiles = _floor_tilemap
	y_sort_node = y_sort
	var used_cells = floor_tiles.get_used_cells()
	var cargo_start_pos : = Vector2.ZERO
	for tile in used_cells:
		var v = floor_tiles.get_cell(tile.x, tile.y)
		if v == CARGO_TILE:
			cargo_tiles.append(tile)
	if cargo_tiles.size() == 0:
		print("ERROR - No cargo floors found in tilemap!")
		return
	
	for i in range(max_cargo):
		# create item ref
		var test = ItemRef.new(Globals.ITEM_TYPES.Cargo)
		var cargo = new_item(test, (get_cargo_pos() * 16) + POS_OFFSET)
		cur_cargo.append(cargo)

func new_item(item_ref : ItemRef, spawn_pos : Vector2, parent : Node2D = null)->Node2D:
	var itm = item_scn.instance()

	itm.set_up(item_ref)
	if parent:
		parent.add_child(itm)  # give it directly to parent
	else:
		y_sort_node.add_child(itm) # add it to the world (on floor)
	itm.global_position = spawn_pos
	return itm
		
func get_cargo_pos()->Vector2:
	var positions = cargo_tiles
	var i = positions.find(last_cargo_pos)
	if i >= 0:
		positions.remove(i)
	positions.shuffle()
	var pos = positions.back()
	last_cargo_pos = pos
	return pos

func remove_cargo(cargo):
	var i = cur_cargo.find(cargo)
	if i < 0: 
		cargo.queue_free()
		return
	cur_cargo.remove(i)
	cargo.queue_free()
	
		
