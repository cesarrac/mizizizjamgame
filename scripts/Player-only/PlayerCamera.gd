extends Camera2D

const LOOK_AHEAD_FACTOR = 0.1
const SHIFT_TRANS = Tween.TRANS_SINE
const SHIFT_EASE = Tween.EASE_OUT
const SHIFT_DURATION = 1.0

onready var prev_pos = get_camera_position()
onready var tween = $ShiftTween
var facing : Vector2 = Vector2.ZERO
var _needs_update : bool = false

export(OpenSimplexNoise) var noise
const MAX_SHAKE_X : = 32
const MAX_SHAKE_Y : = 32
const MAX_ROT = 16
var trauma_listnr : EventListener
var trauma : = 0.0
var time : = 0.0
const TIME_OFFSET = 150.0
const TRAUMA_DECAY_RATE = 0.5

func _ready() -> void:
	trauma_listnr = EventListener.new("add_trauma", self)
	trauma_listnr.subscribe_to("on_trauma", "PlayerCamera")

func add_trauma(value : float):
	trauma = clamp(
		trauma + value,
		0.0,
		1.0
	)

#warning-ignore:unused_argument
func _process(delta):
	_handle_shake(delta)
	
	_check_facing()
	prev_pos = get_camera_position()
	if _needs_update:
		_update_cam()

func _handle_shake(delta):
	time += delta
	var shake = pow(trauma, 2)
	offset.x = noise.get_noise_3d(time * TIME_OFFSET, 0, 0) * MAX_SHAKE_X * shake
	offset.y = noise.get_noise_3d(0, time * TIME_OFFSET, 0) * MAX_SHAKE_Y * shake
	rotation_degrees = noise.get_noise_3d(0, 0, time * TIME_OFFSET) * MAX_ROT * shake
	
	if trauma > 0:
		trauma = clamp(trauma - (delta * TRAUMA_DECAY_RATE), 0.0, 1.0)

func _check_facing():
	var new_x = sign(get_camera_position().x - prev_pos.x)
	if new_x != 0 && facing.x != new_x:
		facing.x = new_x
		_needs_update = true
		
	var new_y = sign(get_camera_position().y - prev_pos.y)
	if new_y != 0 && facing.y != new_y:
		facing.y = new_y
		_needs_update = true


func _update_cam():
	var target_offset = Vector2(get_viewport_rect().size.x * facing.x * LOOK_AHEAD_FACTOR,
									get_viewport_rect().size.y * facing.y * LOOK_AHEAD_FACTOR)
		
	tween.interpolate_property(self, "position", position, target_offset, SHIFT_DURATION, SHIFT_TRANS, SHIFT_EASE)
#		tween.interpolate_property(self, "position:x", position.x, target_offset, SHIFT_DURATION, SHIFT_TRANS, SHIFT_EASE)
	tween.start()
	
	_needs_update = false
	
func reset():
	tween.stop_all()
	position = Vector2.ZERO
	prev_pos = get_camera_position()
#	tween.interpolate_property(self, "position", position, get_parent().position, SHIFT_DURATION, SHIFT_TRANS, SHIFT_EASE)
##		tween.interpolate_property(self, "position:x", position.x, target_offset, SHIFT_DURATION, SHIFT_TRANS, SHIFT_EASE)
#	tween.start()
