extends Node2D

var last_input_vector : Vector2
var input_vector : Vector2
signal on_input(input)
signal on_vector_input(vector)

func _ready():
	connect("on_input", get_parent(), "on_input")
	connect("on_vector_input", get_parent(), "set_input_vector")

func _process(delta):
	if Input.is_action_just_pressed("Equip"):
		emit_signal("on_input", "Equip")
	if Input.is_action_just_pressed("M0"):
		emit_signal("on_input", "Interact")
	if Input.is_action_pressed("M1"):
		emit_signal("on_input", "Drop")
	if Input.is_action_just_released("M0"):
		emit_signal("on_input", "M0_Up")
#	if Input.is_action_just_released("M1"):
#		emit_signal("on_input", "EndAlt")
	if Input.is_action_just_pressed("Interact"):
		emit_signal("on_input", "Interact")
	if Input.is_action_just_pressed("Drop"):
		emit_signal("on_input", "Drop")
		
func _physics_process(delta):
	get_input_vector()
	
func get_input_vector():
	last_input_vector = input_vector
	input_vector = Vector2.ZERO
	if Input.is_action_pressed('Right'):
		input_vector.x += 1
	if Input.is_action_pressed('Left'):
		input_vector.x -= 1
	if Input.is_action_pressed('Up'):
		input_vector.y -= 1
	if Input.is_action_pressed('Down'):
		input_vector.y += 1
	emit_signal("on_vector_input", input_vector)

