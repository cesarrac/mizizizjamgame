extends Node2D

export(OpenSimplexNoise) var noise

onready var timer = $Trauma_Timer
const TRAUMA_RATE = 0.25
var trauma : = 0.0
var time : = 0.0
var danger_level : = 0.0
const MAX_DANGER : = 10.0
const TIME_OFFSET = 10
const TRAUMA_DECAY_RATE = 0.5

func _ready() -> void:
	timer.wait_time = TRAUMA_RATE
	timer.connect("timeout", self, "on_timer_timeout")
	timer.start()

func on_timer_timeout():
	add_trauma()
	timer.start()
	
func add_trauma():
	trauma = clamp(
		trauma + 0.1,
		0.0,
		1.0
	)

func update_danger(delta: float) -> void:
	time += delta
	_handle_danger(delta)

func _handle_danger(delta):
	var danger = pow(trauma, 2)
	var noise_value = noise.get_noise_3d(time * TIME_OFFSET, 0, 0) * danger * 100

	danger_level = clamp(
		danger_level + noise_value,
		0.0,
		MAX_DANGER
	)

	if trauma > 0:
		trauma = clamp(trauma - (delta * TRAUMA_DECAY_RATE), 0.0, 1.0)
	
	if danger_level == MAX_DANGER:
		emit_danger(randi() % 4)
		danger_level = 0.0
		trauma = 0.0
		time = 0.0
		

func emit_danger(danger_type : int):
	var danger_evt = Event.new("on_new_danger", danger_type)
	danger_evt.fire()
	print("emitting danger!")
