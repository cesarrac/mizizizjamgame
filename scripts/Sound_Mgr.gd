extends Node2D

const AUDIO_PATH = "res://assets/audio/"
onready var sound_fx_1 : AudioStreamPlayer2D = $Sound_FX_1
onready var sound_fx_2 : AudioStreamPlayer2D = $Sound_FX_2
onready var music_player : AudioStreamPlayer2D = $MusicPlayer
var sounds : Array

func _ready():
	sounds = _load_sounds(AUDIO_PATH)
#	print("%s sounds loaded" % sounds.size())

func play_sfx(s_name : String, _type : int = 0):
	var sound = _find_snd(s_name, sounds)
	if sound == null:
		print("sound not found")
		return
	if sound_fx_1.playing == true and sound_fx_2.playing == false:
		sound_fx_2.stream = sound
		sound_fx_2.pitch_scale = rand_range(0.8, 1.0)
		sound_fx_2.play()
		return
	sound_fx_1.stream = sound
	sound_fx_1.pitch_scale = rand_range(0.8, 1.0)
	sound_fx_1.play()

func play_music(trk_id : String):
	music_player.stream = _find_snd(trk_id, sounds)
	music_player.play()
	
func _find_snd(_name : String, array : Array):
	for spr in array:
		var sl = spr.resource_path.find("/")
		if sl < 0: continue
		var n = spr.resource_path.find(_name, sl)
		if n < 0: continue
		return spr

func _load_sounds(path : String):
	var files = []
	var dir = Directory.new()
	if dir.open(path) == 0:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while (file_name != ""):
			if dir.current_is_dir():
#				print("Found directory: " + file_name)
				file_name = dir.get_next()
				continue

#			print("Found file: " + file_name)
			files.append(file_name)
			file_name = dir.get_next()
	else:
		print("AudioMgr ERROR -- Cannot access the path: %s" % path)
		return []
	if files.size() < 0:
		print("AudioMgr ERROR -- No files found")
		return []
	var loaded = []
	for fpath in files:
		if fpath.ends_with(".import"):
			fpath = fpath.replace(".import", "")
#			print("replacing import: new name is %s" % fpath)
		var s = load(path+fpath)
		if !s: continue
		loaded.append(s)
	return loaded


func _on_Sound_FX_1_finished():
	sound_fx_1.stop()

func _on_Sound_FX_2_finished():
	sound_fx_2.stop()

func _exit_tree():
	queue_free()
