extends Node2D
class_name Brain

var goals : = []
var cur_state : State
var actor

func set_actor(_actor):
	actor = _actor
	push_goal(IdleState.new(actor))
	cur_state = goals.back()
	cur_state.enter(actor)
	print("actor set")
	
func set_input_vector(vector : Vector2):
	if !cur_state: return
	cur_state.on_vector_input(actor, vector)

func set_input(input):
	if !cur_state: return
	cur_state.on_input(actor, input)

func _process(delta):
	if !actor: return
	get_next_state()
	cur_state.update(actor, delta)

func _physics_process(delta):
	if !cur_state: return
	cur_state.physics_process(actor, delta)

func get_next_state():
	while(goals.back().is_finished(actor) == true):
		goals.back().exit(actor)
		goals.remove(goals.size() - 1)
		
	if cur_state and cur_state != goals.back():
		goals.back().enter(actor)
		
	cur_state = goals.back()

func push_goal(goal):
	if goals.size() > 0 and goals.back().state_name == goal.state_name:
		return
	# interrupt current state
	if goals.size() > 0:
		goals.back().interrupt()
	
	goals.push_back(goal)
#	print("pushing %s state" % goal.state_name)
	
func peek_current():
	return goals.back()

func finish_current():
	goals.back().finish()

func wait_complete():
	goals.back().wait_complete()
