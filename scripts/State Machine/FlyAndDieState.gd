extends State
class_name FlyDieState

var move_comp
var input_vector : = Vector2.ZERO

func _init(actor).("FlyDie", actor):
	move_comp = actor.node.movement

func enter(actor):
	actor.node.is_attacking = false
		
func on_vector_input(actor, vector):
	input_vector = vector.normalized()
	if input_vector == Vector2.ZERO:
		finish()
		return
	else:
		actor.node.animate("Run")
	
func physics_process(actor, delta):
	move_comp.move(input_vector, delta)
