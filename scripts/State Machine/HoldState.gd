extends State
class_name HoldState

var move_comp
var input_vector = Vector2.ZERO

func _init(actor).("Hold", actor):
	move_comp = actor.node.movement

func enter(actor):
	actor.actor_state = Actor.ACTOR_STATE.Held
	actor.node.animate("Choke")

func on_input(actor, input):
	pass
	
func update(actor, delta):
	pass

#func physics_process(actor, delta):
#	move_comp.move(input_vector, delta)

func is_finished(actor)->bool:
	return _finished

func finish():
	_finished = true

func interrupt():
	pass

func exit(actor):
	pass

func animation_finished(anim_name : String):
	pass
