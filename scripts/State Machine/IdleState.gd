extends State
class_name IdleState

var move_comp

func _init(actor).("Idle", actor):
	move_comp = actor.node.movement

func enter(actor):
	actor.actor_state = Actor.ACTOR_STATE.Active
	actor.node.animate("Idle")

func on_vector_input(actor, vector):
	if vector == Vector2.ZERO:
		return
	actor.node.brain.push_goal(MoveState.new(actor))

func physics_process(actor, delta):
	move_comp.move(Vector2.ZERO, delta)
	
func is_finished(actor)->bool:
	return false

