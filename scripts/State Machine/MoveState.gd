extends State
class_name MoveState

var move_comp
var input_vector : = Vector2.ZERO

func _init(actor).("Move", actor):
	move_comp = actor.node.movement

func enter(actor):
	input_vector = actor.node.input_vector
		
func on_vector_input(actor, vector):
	input_vector = vector.normalized()
	if input_vector == Vector2.ZERO:
#		print("no input vector")
		finish()
		return
	else:
		actor.node.animate("Run")
	
func physics_process(actor, delta):
	move_comp.move(input_vector, delta)
	
