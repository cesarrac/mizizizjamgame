extends State
class_name UsePowerState

var move_comp 

func _init(actor).("Power", actor):
	move_comp = actor.node.movement

func enter(actor):
	pass

func on_input(actor, input):
	pass

func on_vector_input(actor, vector):
	pass
	
func update(actor, delta):
	pass

func physics_process(actor, delta):
	move_comp.move(Vector2.ZERO, delta)

func is_finished(actor)->bool:
	return _finished

func finish():
	_finished = true

func interrupt():
	pass

func exit(actor):
	pass

func animation_finished(anim_name : String):
	pass
