extends State
class_name ShootState

var move_comp
var laser_comp

var shots_fired : = 0
var max_shots : = 1

func _init(actor).("Shoot", actor):
	move_comp = actor.node.movement
	laser_comp = actor.node.laser

func enter(actor):
	if actor.node.aim_position == Vector2.ZERO:
		finish()
		return
	max_shots = randi() % 4
	actor.node.attack()

func on_input(actor, input):
	if input == "Attack":
		next_attack(actor)

func next_attack(actor):
	if shots_fired >= max_shots:
		actor.node.brain.push_goal(FlyDieState.new(actor))
		finish()
	else:
		actor.node.attack()

func on_vector_input(actor, vector):
	pass
	
func update(actor, delta):
	pass

func physics_process(actor, delta):
	move_comp.move(Vector2.ZERO, delta)

func is_finished(actor)->bool:
	return _finished

func finish():
	_finished = true

func interrupt():
	pass

func exit(actor):
	pass

func animation_finished(anim_name : String):
	pass
