extends Reference
class_name State

var state_name : String
var _finished : bool

func _init(_name : String, actor : Actor):
	state_name = _name

func enter(actor):
	pass

func on_input(actor, input):
	pass

func on_vector_input(actor, vector):
	pass
	
func update(actor, delta):
	pass

func physics_process(actor, delta):
	pass

func is_finished(actor)->bool:
	return _finished

func finish():
	_finished = true

func interrupt():
	pass

func exit(actor):
	pass

func animation_finished(anim_name : String):
	pass
